---
title: about
layout: default
---
About Adam Green
================

I am a graduate student at University of Colorado at Boulder in the Soft Matter
Research Group. I work in the Thin Films lab, where I study both the hydrodynamics
and the different phases of liquid crystals.


